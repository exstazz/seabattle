using System;

namespace GameForBattleShip
{
    class BattleshipBoard
    {
        public void DisplayBoard(char[,] Board, char[,] BoardPc)
        {
            int Row;
            int Column;

            //Поле персонажа
            Console.WriteLine("Ваше поле\n");
            Console.WriteLine("  ¦ A B C D E F G H I J");
            Console.WriteLine("--+--------------------");
            for (Row = 0; Row <= 9; Row++)
            {
                if (Row + 1 == 10)
                {
                    Console.Write((Row + 1).ToString() + "¦ ");
                }

                else
                {
                    Console.Write((Row + 1).ToString() + " ¦ ");
                }
                for (Column = 0; Column <= 9; Column++)
                {
                    Console.Write(Board[Column, Row] + " ");
                }
                Console.WriteLine();
            }



            //Поле робота
            Console.WriteLine("Поле робота : \n");
            Console.WriteLine("  ¦ A B C D E F G H I J");
            Console.WriteLine("--+--------------------");
            for (Row = 0; Row <= 9; Row++)
            {
                if (Row + 1 == 10)
                {
                    Console.Write((Row + 1).ToString() + "¦ ");
                }

                else
                {
                    Console.Write((Row + 1).ToString() + " ¦ ");
                }
                for (Column = 0; Column <= 9; Column++)
                {
                    Console.Write(BoardPc[Column, Row] + " ");
                }
                Console.WriteLine();
            }
        }
    }
    class Compucter
    {
        public int ResAboutCompucter = 0;
        public bool Shot = false;
        public bool ShotRight = false;
        public bool ShotLeft = false;
        public bool ShotUp = false;
        public bool ShotDown = false;
        public int CheckLvlCreatedShips = 0;
        public int horizontal;
        public int lentgh;
        public int x;
        public int y;

        public void ProvShots(int column, int row)
        {
            MyPlayer myPlayer = new MyPlayer();
            if (myPlayer.Grid[column - 1, row - 1].Equals('*'))
            {
                Shot = true;
                ResAboutCompucter++;
                myPlayer.Grid[column - 1, row - 1] = 'X';
                Console.WriteLine("Попадание со стороны робота:(\n");
            }
            else
            {
                myPlayer.Grid[column - 1, row - 1] = 'O';
                Console.WriteLine("Промах со стороны робота:)\r\n");
            }
        }
        public void RandomShot()
        {
            Random rnd = new Random();
            int column = 0;
            int row = 0;
            if (Shot == true)
            {
                if (ShotRight != true)
                {
                    ProvShots(column + 1, row);
                    ShotRight = false;
                }
                else if (ShotLeft != true)
                {
                    ProvShots(column - 1, row);
                    ShotLeft = false;
                }
                else if (ShotUp != true)
                {
                    ProvShots(column, row + 1);
                    ShotUp = false;
                }
                else if (ShotDown != true)
                {
                    ProvShots(column, row - 1);
                    ShotDown = false;
                    Shot = false;
                }
            }
            else
            {
                column = rnd.Next(1, 11);
                row = rnd.Next(1, 11);
                ProvShots(column, row);
            }
        }
        public void SpawnCompucterShip()
        {
            Random rnd = new Random();
            horizontal = rnd.Next(1, 3);
            if (horizontal == 1)//h
            {
                CheckLvlCreatedShips++;
                x = rnd.Next(1, 3);
                y = rnd.Next(1, 11);
                SetGridAboutPc(x, y);
                SetGridAboutPc(x + 1, y);
                SetGridAboutPc(x + 2, y);
                SetGridAboutPc(x + 3, y);
                
            }
            else//v
            {
                CheckLvlCreatedShips++;
                x = rnd.Next(1, 11);
                y = rnd.Next(1, 4);
                SetGridAboutPc(x, y);
                SetGridAboutPc(x, y + 1);
                SetGridAboutPc(x, y + 2);
                SetGridAboutPc(x, y + 3);
            }
            for (int i = 0; i < 3; i++)
            {
                horizontal = rnd.Next(1, 3);
                if (horizontal == 1)//h
                {
                    CheckLvlCreatedShips++;
                    x = rnd.Next(1, 6);
                    y = rnd.Next(1, 11);
                    SetGridAboutPc(x, y);
                    SetGridAboutPc(x + 1, y);
                    SetGridAboutPc(x + 2, y);
                }
                else//v
                {
                    CheckLvlCreatedShips++;
                    x = rnd.Next(1, 11);
                    y = rnd.Next(1, 7);
                    SetGridAboutPc(x, y);
                    SetGridAboutPc(x, y + 1);
                    SetGridAboutPc(x, y + 2);
                }
            }
            for (int i = 0; i < 4; i++)
            {
                if (horizontal == 1)//h
                {
                    CheckLvlCreatedShips++;
                    x = rnd.Next(1, 9);
                    y = rnd.Next(1, 11);
                    SetGridAboutPc(x, y);
                    SetGridAboutPc(x + 1, y);
                }
                else//v
                {
                    CheckLvlCreatedShips++;
                    x = rnd.Next(1, 11);
                    y = rnd.Next(1, 10);
                    SetGridAboutPc(x, y);
                    SetGridAboutPc(x, y + 1);
                }
            }
            for (int i = 0; i < 5; i++)
            {
                horizontal = rnd.Next(1, 3);
                if (horizontal == 1)//h
                {
                    CheckLvlCreatedShips++;
                    x = rnd.Next(1, 11);
                    y = rnd.Next(1, 11);
                    SetGridAboutPc(x, y);
                }
                else//v
                {
                    CheckLvlCreatedShips++;
                    povtlvl();
                }
            }
        }
        public void povtlvl()
        {
            Random rnd = new Random();
            if (CheckLvlCreatedShips >= 2 && CheckLvlCreatedShips <= 3)
            {
                lentgh = 3;
                if (horizontal == 1)//h
                {
                    horizontal = rnd.Next(1, 3);
                    if (horizontal == 1)//h
                    {
                        CheckLvlCreatedShips++;
                        x = rnd.Next(1, 6);
                        y = rnd.Next(1, 11);
                        SetGridAboutPc(x, y);
                        SetGridAboutPc(x + 1, y);
                        SetGridAboutPc(x + 2, y);
                    }
                    else//v
                    {
                        CheckLvlCreatedShips++;
                        x = rnd.Next(1, 11);
                        y = rnd.Next(1, 7);
                        SetGridAboutPc(x, y);
                        SetGridAboutPc(x, y + 1);
                        SetGridAboutPc(x, y + 2);
                    }
                }
                if (CheckLvlCreatedShips >= 4 && CheckLvlCreatedShips <= 6)
                {
                    lentgh = 2;
                    if (horizontal == 1)//h
                    {
                        CheckLvlCreatedShips++;
                        x = rnd.Next(1, 9);
                        y = rnd.Next(1, 11);
                        SetGridAboutPc(x, y);
                        SetGridAboutPc(x + 1, y);
                    }
                    else//v
                    {
                        CheckLvlCreatedShips++;
                        x = rnd.Next(1, 11);
                        y = rnd.Next(1, 10);
                        SetGridAboutPc(x, y);
                        SetGridAboutPc(x, y + 1);
                    }
                }
                if (CheckLvlCreatedShips >= 7 && CheckLvlCreatedShips <= 10)
                {
                    lentgh = 1;
                    horizontal = rnd.Next(1, 3);
                    if (horizontal == 1)//h
                    {
                        CheckLvlCreatedShips++;
                        x = rnd.Next(1, 11);
                        y = rnd.Next(1, 11);
                        SetGridAboutPc(x, y);
                    }
                    else//v
                    {
                        CheckLvlCreatedShips++;
                        x = rnd.Next(1, 11);
                        y = rnd.Next(1, 11);
                        SetGridAboutPc(x, y);
                    }
                }
            }
        }
        public void CheckAboutShip(int q, int w)
        {
            MyPlayer myPlayer = new MyPlayer();
            if (lentgh == 4)
            {
                if (horizontal == 1)
                {
                    SetGridAboutPc(q, w);
                    SetGridAboutPc(q + 1, w);
                    SetGridAboutPc(q + 2, w);
                    SetGridAboutPc(q + 3, w);
                }
                else if (horizontal == 2)
                {
                    SetGridAboutPc(q, w);
                    SetGridAboutPc(q, w + 1);
                    SetGridAboutPc(q, w + 2);
                    SetGridAboutPc(q, w + 3);
                }
            }
            //Еси длинна равна 3
            if (lentgh == 3)
            {
                if (horizontal == 1)
                {
                    if (myPlayer.GridPc[q, w] == '*')
                    {
                        povtlvl();
                    }
                    else if (myPlayer.GridPc[q + 1, w] == '*')
                    {
                        povtlvl();
                    }
                    else if (myPlayer.GridPc[q + 2, w] == '*')
                    {
                        povtlvl();
                    }
                    else
                    {
                        SetGridAboutPc(q, w);
                        SetGridAboutPc(q + 1, w);
                        SetGridAboutPc(q + 2, w);
                    }
                }
                else if (horizontal == 2)
                {
                    if (myPlayer.GridPc[q, w - 1] == '*')
                    {
                        povtlvl();
                    }
                    else if (myPlayer.GridPc[q, w+ 1] == '*')
                    {
                        povtlvl();
                    }
                    else if (myPlayer.GridPc[q, w+ 2] == '*')
                    {
                        povtlvl();
                    }
                    else
                    {
                        SetGridAboutPc(q, w);
                        SetGridAboutPc(q, w + 1);
                        SetGridAboutPc(q, w + 2);
                    }
                }
            }
            //Еси длинна равна 3
            if (lentgh == 2)
            {
                if (horizontal == 1)
                {
                    if (myPlayer.GridPc[q, w] == '*')
                    {
                        povtlvl();
                    }
                    else if (myPlayer.GridPc[q + 1, w] == '*')
                    {
                        povtlvl();
                    }
                    else
                    {
                        SetGridAboutPc(q, w);
                        SetGridAboutPc(q + 1, w);
                    }
                }
                else if (horizontal == 2)
                {
                    if (myPlayer.GridPc[q, w] == '*')
                    {
                        povtlvl();
                    }
                    else if (myPlayer.GridPc[q, w + 1] == '*')
                    {
                        povtlvl();
                    }
                    else
                    {
                        SetGridAboutPc(q, w);
                        SetGridAboutPc(q, w + 1);
                    }
                }
            }
            else if(lentgh == 1)
            {
                if (myPlayer.GridPc[q, w] == '*')
                {
                    povtlvl();
                }
                else
                {
                    SetGridAboutPc(q, w);
                }
            }
        }
        public void SetGridAboutPc(int q, int w)
        {
            MyPlayer myPlayer = new MyPlayer();
            myPlayer.GridPc[q - 1, w -1] = '*';
        }
    }
        class MyPlayer
        {
            public int ResAboutPeople = 0;
            public string horizontal;
            public string Line;
            public char[,] Grid = new char[10, 10];
            public char[,] GridPc = new char[10, 10];
            public int x = 0;
            public int y = 0;
            public int q;
            public int w;
            public int ValueAboutCountExp = 0;
            public int lentgh;

            public void AskCoordinates()
            {
                Console.WriteLine("Введите X(A-J)");
                Line = Console.ReadLine();
                Replace();
                int value;
                if (int.TryParse(Line, out value))
                {
                    x = value;
                }
                else
                {
                    Console.WriteLine("Error : Соблядайте правила, вводите , букву от A до J!");
                    AskCoordinates();
                }

                Console.WriteLine("Введите Y(1-10)");
                Line = Console.ReadLine();
                Replace();
                if (int.TryParse(Line, out value))
                {
                    y = value;
                }
                else
                {
                    Console.WriteLine("Error : Соблядайте правила, вводите число от 1 до 10!");
                    AskCoordinates();
                }

                try
                {
                    if (GridPc[x, y - 1].Equals('*'))
                    {
                        ResAboutPeople++;
                        GridPc[x, y - 1] = 'X';
                        Console.WriteLine("Попадание:)\n");
                    }
                    else
                    {
                        GridPc[x, y - 1] = 'O';
                        Console.WriteLine("Промах:(\r\n");
                    }
                }
                catch
                {
                    Console.WriteLine("Error: Соблядайте правила, вводите число от 1 до 10!");
                    AskCoordinates();
                }
            }
            public void Replace()
            {
                Line = Line.Replace("A", "0");
                Line = Line.Replace("B", "1");
                Line = Line.Replace("C", "2");
                Line = Line.Replace("D", "3");
                Line = Line.Replace("E", "4");
                Line = Line.Replace("F", "5");
                Line = Line.Replace("G", "6");
                Line = Line.Replace("H", "7");
                Line = Line.Replace("I", "8");
                Line = Line.Replace("J", "9");
            }
            public void SetGrid(int q, int w)
            {
                Grid[q, w - 1] = '*';
            }
            public void GetStartedCardinates()
            {
                GetStartedCardinatesAbout4Ship();
                GetStartedCardinatesAbout3Ship();
                GetStartedCardinatesAbout2Ship();
                GetStartedCardinatesAbout1Ship();
            }
            public void GetStartedCardinatesAbout4Ship()
            {
                lentgh = 4;
                ValueAboutCountExp++;
                PovtExp();
            }

            public void GetStartedCardinatesAbout3Ship()
            {
                lentgh = 3;
                //1
                ValueAboutCountExp++;
                PovtExp();
                //2
                ValueAboutCountExp++;
                PovtExp();
            }
            public void GetStartedCardinatesAbout2Ship()
            {
                lentgh = 2;
                //1
                ValueAboutCountExp++;
                PovtExp();
                //2
                ValueAboutCountExp++;
                PovtExp();
                //3
                ValueAboutCountExp++;
                PovtExp();
            }
            public void GetStartedCardinatesAbout1Ship()
            {
                lentgh = 1;
                ValueAboutCountExp++;
                PovtExp();
                //2
                ValueAboutCountExp++;
                PovtExp();
                //3
                ValueAboutCountExp++;
                PovtExp();
                //4
                ValueAboutCountExp++;
                PovtExp();
            }
            public void CheckForHErrorForPlayer(string h)
            {
                if (h == "v" || h == "h")
                {
                }
                else
                {
                    Console.Write("Вы ввели некоректные данные. Введите корректировки в ваши данные.\r\n");
                    PovtExp();
                }
            }
            public void CheckForQErrorForPlayer(string q)
            {
                if (q == "A" || q == "B" || q == "C" || q == "D" || q == "E" || q == "F" || q == "G" || q == "H" || q == "I" || q == "J")
                {
                    if (horizontal == "h")
                    {
                        if (lentgh == 4)
                        {
                            if (q == "H" || q == "I" || q == "J")
                            {
                                Console.WriteLine("Корабль не может выходить за поле. Попробуйте ввести кординаты ещё раз.");
                                PovtExp();
                            }
                        }
                        if (lentgh == 3)
                        {
                            if (q == "I" || q == "J")
                            {
                                Console.WriteLine("Корабль не может выходить за поле. Попробуйте ввести кординаты ещё раз.");
                                PovtExp();
                            }
                        }
                        if (lentgh == 2)
                        {
                            if (q == "J")
                            {
                                Console.WriteLine("Корабль не может выходить за поле. Попробуйте ввести кординаты ещё раз.");
                                PovtExp();
                            }
                        }
                    }
                    //Не нужно проверять другую полярность так как по буквенным кардинатам за зону выйти нельзя только если ввести другую букву но я уже это проверил
                }
                else
                {
                    Console.Write("Вы ввели некоректные данные. Введите корректировки в ваши данные.\r\n");
                    PovtExp();
                }
            }
            public void CheckForWErrorForPlayer(string w)
            {
                if (w == "1" || w == "2" || w == "3" || w == "4" || w == "5" || w == "6" || w == "7" || w == "8" || w == "9" || w == "10")
                {
                    if (horizontal == "v")
                    {
                        if (lentgh == 4)
                        {
                            if (w == "8" || w == "9" || w == "10")
                            {
                                Console.WriteLine("Корабль не может выходить за поле. Попробуйте ввести кординаты ещё раз.");
                                PovtExp();
                            }
                        }
                        if (lentgh == 3)
                        {
                            if (w == "9" || w == "10")
                            {
                                Console.WriteLine("Корабль не может выходить за поле. Попробуйте ввести кординаты ещё раз.");
                                PovtExp();
                            }
                        }
                        if (lentgh == 2)
                        {
                            if (w == "10")
                            {
                                Console.WriteLine("Корабль не может выходить за поле. Попробуйте ввести кординаты ещё раз.");
                                PovtExp();
                            }
                        }
                    }
                }
                else
                {
                    Console.Write("Вы ввели некоректные данные. Введите корректировки в ваши данные.\r\n");
                    PovtExp();
                }
            }
            public void PovtExp()
            {
                if (ValueAboutCountExp == 1)
                {
                    lentgh = 4;
                    Console.WriteLine("Введите как будет распологаться четырёхпалубный корабль : Вертикально(v) или горизонтально(h)");
                    Console.WriteLine("Пример ввода : v");
                    horizontal = Console.ReadLine();
                    CheckForHErrorForPlayer(horizontal);
                    Console.WriteLine("Введите начальную кардинату x для четырёхпалубного коробля : от A до J");
                    Console.WriteLine("Пример ввода : F");
                    Line = Console.ReadLine();
                    CheckForQErrorForPlayer(Line);
                    Replace();
                    q = Convert.ToInt32(Line);
                    Console.WriteLine("Введите начальную кардинату y для четырёхпалубного коробля : от 1 до 10");
                    Console.WriteLine("Пример ввода : 4");
                    Line = Console.ReadLine();
                    CheckForWErrorForPlayer(Line);
                    Replace();
                    w = Convert.ToInt32(Line);
                    CheckFor1Cel(q, w);
                    CheckErrorAboutManyCell();
                }
                else if (ValueAboutCountExp == 2)
                {
                    lentgh = 3;
                    Console.WriteLine("Введите как будет распологаться первый трёхпалубный корабль : Вертикально(v) или горизонтально(h)");
                    Console.WriteLine("Пример ввода : v");
                    horizontal = Console.ReadLine();
                    CheckForHErrorForPlayer(horizontal);
                    Console.WriteLine("Введите начальную кардинату x для первого трёхпалубного корабля : от A до J");
                    Console.WriteLine("Пример ввода : F");
                    Line = Console.ReadLine();
                    CheckForQErrorForPlayer(Line);
                    Replace();
                    q = Convert.ToInt32(Line);
                    Console.WriteLine("Введите начальную кардинату y для первого трёхпалубного корабля : от 1 до 10");
                    Console.WriteLine("Пример ввода : 4");
                    Line = Console.ReadLine();
                    CheckForWErrorForPlayer(Line);
                    Replace();
                    w = Convert.ToInt32(Line);
                    CheckErrorAboutManyCell();
                }
                else if (ValueAboutCountExp == 3)
                {
                    lentgh = 3;
                    Console.WriteLine("Введите как будет распологаться второй трёхпалубный корабль : Вертикально(v) или горизонтально(h)");
                    Console.WriteLine("Пример ввода : v");
                    horizontal = Console.ReadLine();
                    CheckForHErrorForPlayer(horizontal);
                    Console.WriteLine("Введите начальную кардинату x для второго трёхпалубного корабля : от A до J");
                    Console.WriteLine("Пример ввода : F");
                    Line = Console.ReadLine();
                    CheckForQErrorForPlayer(Line);
                    Replace();
                    q = Convert.ToInt32(Line);
                    Console.WriteLine("Введите начальную кардинату y для второго трёхпалубного корабля : от 1 до 10");
                    Console.WriteLine("Пример ввода : 4");
                    Line = Console.ReadLine();
                    CheckForWErrorForPlayer(Line);
                    Replace();
                    w = Convert.ToInt32(Line);
                    CheckFor1Cel(q, w);
                    CheckErrorAboutManyCell();
                }
                else if (ValueAboutCountExp == 4)
                {
                    lentgh = 2;
                    Console.WriteLine("Введите как будет распологаться первый двухпалубный корабль : Вертикально(v) или горизонтально(h)");
                    Console.WriteLine("Пример ввода : v");
                    horizontal = Console.ReadLine();
                    CheckForHErrorForPlayer(horizontal);
                    Console.WriteLine("Введите начальную кардинату x для первого двухпалубного корабля : от A до J");
                    Console.WriteLine("Пример ввода : F");
                    Line = Console.ReadLine();
                    CheckForQErrorForPlayer(Line);
                    Replace();
                    q = Convert.ToInt32(Line);
                    Console.WriteLine("Введите начальную кардинату y для первого двухпалубного корабля : от 1 до 10");
                    Console.WriteLine("Пример ввода : 4");
                    Line = Console.ReadLine();
                    CheckForWErrorForPlayer(Line);
                    w = Convert.ToInt32(Line);
                    CheckFor1Cel(q, w);
                    CheckErrorAboutManyCell();
                }
                else if (ValueAboutCountExp == 5)
                {
                    lentgh = 2;
                    Console.WriteLine("Введите как будет распологаться второй двухпалубный корабль : Вертикально(v) или горизонтально(h)");
                    Console.WriteLine("Пример ввода : v");
                    horizontal = Console.ReadLine();
                    CheckForHErrorForPlayer(horizontal);
                    Console.WriteLine("Введите начальную кардинату x для второго двухпалубного корабля : от A до J");
                    Console.WriteLine("Пример ввода : F");
                    Line = Console.ReadLine();
                    CheckForQErrorForPlayer(Line);
                    Replace();
                    q = Convert.ToInt32(Line);
                    Console.WriteLine("Введите начальную кардинату y для второго двухпалубного корабля : от 1 до 10");
                    Console.WriteLine("Пример ввода : 4");
                    Line = Console.ReadLine();
                    CheckForWErrorForPlayer(Line);
                    w = Convert.ToInt32(Line);
                    CheckFor1Cel(q, w);
                    CheckErrorAboutManyCell();
                }
                else if (ValueAboutCountExp == 6)
                {
                    lentgh = 2;
                    Console.WriteLine("Введите как будет распологаться третий двухпалубный корабль : Вертикально(v) или горизонтально(h)");
                    Console.WriteLine("Пример ввода : v");
                    horizontal = Console.ReadLine();
                    CheckForHErrorForPlayer(horizontal);
                    Console.WriteLine("Введите начальную кардинату x для третьего двухпалубного корабля : от A до J");
                    Console.WriteLine("Пример ввода : F");
                    Line = Console.ReadLine();
                    CheckForQErrorForPlayer(Line);
                    Replace();
                    q = Convert.ToInt32(Line);
                    Console.WriteLine("Введите начальную кардинату y для третьего двухпалубного корабля : от 1 до 10");
                    Console.WriteLine("Пример ввода : 4");
                    Line = Console.ReadLine();
                    CheckForWErrorForPlayer(Line);
                    w = Convert.ToInt32(Line);
                    CheckFor1Cel(q, w);
                    CheckErrorAboutManyCell();
                }
                else if (ValueAboutCountExp == 7)
                {
                    lentgh = 1;
                    Console.WriteLine("Введите начальную кардинату x для первого однопалубного корабля : от A до J");
                    Console.WriteLine("Пример ввода : F");
                    Line = Console.ReadLine();
                    CheckForQErrorForPlayer(Line);
                    Replace();
                    q = Convert.ToInt32(Line);
                    Console.WriteLine("Введите начальную кардинату y для первого однопалубного корабля : от 1 до 10");
                    Console.WriteLine("Пример ввода : 4");
                    Line = Console.ReadLine();
                    CheckForWErrorForPlayer(Line);
                    w = Convert.ToInt32(Line);
                    CheckFor1Cel(q, w);
                    CheckErrorAboutManyCell();
                }
                else if (ValueAboutCountExp == 8)
                {
                    lentgh = 1;
                    Console.WriteLine("Введите начальную кардинату x для второго однопалубного корабля : от A до J");
                    Console.WriteLine("Пример ввода : F");
                    Line = Console.ReadLine();
                    CheckForQErrorForPlayer(Line);
                    Replace();
                    q = Convert.ToInt32(Line);
                    Console.WriteLine("Введите начальную кардинату y для второго однопалубного корабля : от 1 до 10");
                    Console.WriteLine("Пример ввода : 4");
                    Line = Console.ReadLine();
                    CheckForWErrorForPlayer(Line);
                    w = Convert.ToInt32(Line);
                    CheckFor1Cel(q, w);
                    CheckErrorAboutManyCell();
                }
                else if (ValueAboutCountExp == 9)
                {
                    lentgh = 1;
                    Console.WriteLine("Введите начальную кардинату x для третьего однопалубного корабля : от A до J");
                    Console.WriteLine("Пример ввода : F");
                    Line = Console.ReadLine();
                    CheckForQErrorForPlayer(Line);
                    Replace();
                    q = Convert.ToInt32(Line);
                    Console.WriteLine("Введите начальную кардинату y для третьего однопалубного корабля : от 1 до 10");
                    Console.WriteLine("Пример ввода : 4");
                    Line = Console.ReadLine();
                    CheckForWErrorForPlayer(Line);
                    w = Convert.ToInt32(Line);
                    CheckFor1Cel(q, w);
                    CheckErrorAboutManyCell();
                }
                else if (ValueAboutCountExp == 10)
                {
                    lentgh = 1;
                    Console.WriteLine("Введите начальную кардинату x для четвёртого однопалубного корабля : от A до J");
                    Console.WriteLine("Пример ввода : F");
                    Line = Console.ReadLine();
                    CheckForQErrorForPlayer(Line);
                    Replace();
                    q = Convert.ToInt32(Line);
                    Console.WriteLine("Введите начальную кардинату y для четвёртого однопалубного корабля : от 1 до 10");
                    Console.WriteLine("Пример ввода : 4");
                    Line = Console.ReadLine();
                    CheckForWErrorForPlayer(Line);
                    w = Convert.ToInt32(Line);
                    CheckFor1Cel(q, w);
                    CheckErrorAboutManyCell();
                }
            }
            public void CheckErrorAboutManyCell()
            {
                if (lentgh == 4)
                {
                    if (horizontal == "h")
                    {
                        SetGrid(q, w);
                        SetGrid(q + 1, w);
                        SetGrid(q + 2, w);
                        SetGrid(q + 3, w);
                    }
                    else if (horizontal == "v")
                    {
                        SetGrid(q, w);
                        SetGrid(q, w + 1);
                        SetGrid(q, w + 2);
                        SetGrid(q, w + 3);
                    }
                }
                //Еси длинна равна 3
                if (lentgh == 3)
                {
                    if (horizontal == "h")
                    {
                        if (Grid[q, w - 1] == '*')
                        {
                            Console.WriteLine("На этом месте уже располагается часть корабля. Попробуйте снова!");
                            PovtExp();
                        }
                        else if (Grid[q + 1, w - 1] == '*')
                        {
                            Console.WriteLine("На этом месте уже располагается часть корабля. Попробуйте снова!");
                            PovtExp();
                        }
                        else if (Grid[q + 2, w - 1] == '*')
                        {
                            Console.WriteLine("На этом месте уже располагается часть корабля. Попробуйте снова!");
                            PovtExp();
                        }
                        else
                        {
                            SetGrid(q, w);
                            SetGrid(q + 1, w);
                            SetGrid(q + 2, w);
                        }
                    }
                    else if (horizontal == "v")
                    {
                        if (Grid[q, w - 1] == '*')
                        {
                            Console.WriteLine("На этом месте уже располагается часть корабля. Попробуйте снова!");
                            PovtExp();
                        }
                        else if (Grid[q, w - 1 + 1] == '*')
                        {
                            Console.WriteLine("На этом месте уже располагается часть корабля. Попробуйте снова!");
                            PovtExp();
                        }
                        else if (Grid[q, w - 1 + 2] == '*')
                        {
                            Console.WriteLine("На этом месте уже располагается часть корабля. Попробуйте снова!");
                            PovtExp();
                        }
                        else
                        {
                            SetGrid(q, w);
                            SetGrid(q, w + 1);
                            SetGrid(q, w + 2);
                        }
                    }
                }

                //Еси длинна равна 2
                if (lentgh == 2)
                {
                    if (horizontal == "h")
                    {
                        if (Grid[q, w - 1] == '*')
                        {
                            Console.WriteLine("На этом месте уже располагается часть корабля. Попробуйте снова!");
                            PovtExp();
                        }
                        else if (Grid[q + 1, w - 1] == '*')
                        {
                            Console.WriteLine("На этом месте уже располагается часть корабля. Попробуйте снова!");
                            PovtExp();
                        }
                        else
                        {
                            SetGrid(q, w);
                            SetGrid(q + 1, w);
                        }
                    }
                    if (horizontal == "v")
                    {
                        if (Grid[q, w - 1] == '*')
                        {
                            Console.WriteLine("На этом месте уже располагается часть корабля. Попробуйте снова!");
                            PovtExp();
                        }
                        else if (Grid[q, w - 1 + 1] == '*')
                        {
                            Console.WriteLine("На этом месте уже располагается часть корабля. Попробуйте снова!");
                            PovtExp();
                        }
                        else
                        {
                            SetGrid(q, w);
                            SetGrid(q, w + 1);
                        }
                    }
                }
                //Еси длинна равна 1
                if (lentgh == 1)
                {
                    if (Grid[q, w - 1] == '*')
                    {
                        Console.WriteLine("На этом месте уже располагается часть корабля. Попробуйте снова!");
                        PovtExp();
                    }
                    else
                    {
                        SetGrid(q, w);
                    }
                }
            }
            public void CheckFor1Cel(int q, int w)
            {
                if (q == 1)
                {
                    if (Grid[q + 1, w] == '*' || Grid[q, w - 1] == '*' || Grid[q, w + 1] == '*')
                    {
                        Console.WriteLine("Между кораблям нету расстояния одной клетки");
                        PovtExp();
                    }
                }
                else if (w == 10)
                {
                    if (Grid[q - 1, w] == '*' || Grid[q, w - 1] == '*' || Grid[q, w + 1] == '*')
                    {
                        Console.WriteLine("Между кораблям нету расстояния одной клетки");
                        PovtExp();
                    }
                }
                if (q == 9)
                {
                    if (Grid[q - 1, w] == '*' || Grid[q, w - 1] == '*' || Grid[q, w + 1] == '*')
                    {
                        Console.WriteLine("Между кораблям нету расстояния одной клетки");
                        PovtExp();
                    }
                }
                else if (w == 1)
                {
                    if (Grid[q + 1, w] == '*' || Grid[q, w - 1] == '*' || Grid[q, w + 1] == '*')
                    {
                        Console.WriteLine("Между кораблям нету расстояния одной клетки");
                        PovtExp();
                    }
                }
            }
        }


        class Program
        {
            static void Main(string[] args)
            {

                Console.WriteLine("Добро пожаловать в Морской Бой\r\n");
                Console.WriteLine("\r\n");
                BattleshipBoard b = new BattleshipBoard();
                Compucter c = new Compucter();
                MyPlayer p = new MyPlayer();
                p.GetStartedCardinates();
                c.SpawnCompucterShip();
                for (int i = 0; i < 100; i++)
                {
                    b.DisplayBoard(p.Grid, p.GridPc);
                    p.AskCoordinates();
                    c.RandomShot();
                    if (p.ResAboutPeople == 10)
                    {
                        Console.WriteLine("Вы проиграли.");
                        break;

                    }
                    else if (c.ResAboutCompucter == 10)
                    {
                        Console.WriteLine("Вы выйграли.");
                        break;
                    }

                }
                Console.ReadLine();
            }
        }
    }

